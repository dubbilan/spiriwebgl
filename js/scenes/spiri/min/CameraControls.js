/**
 * @author Lasse Juul Kolding / http://dubbilan.net/
 */
function CamerasSetup(){cameraOrbit=new THREE.PerspectiveCamera(35,SCREEN_WIDTH/SCREEN_HEIGHT,1,1e4),cameraOrbit.position.set(700,200,700)}function ControlsSetUp(){controlsOrbit=new THREE.OrbitControls(cameraOrbit,container),controlsOrbit.target=new THREE.Vector3(0,0,0),controlsOrbit.minPolarAngle=0,controlsOrbit.maxPolarAngle=.9*Math.PI,controlsOrbit.enablePan=!1,controlsOrbit.zoomSpeed=.5,controlsOrbit.minDistance=600,controlsOrbit.maxDistance=2e3,controlsOrbit.enableKeys=!1,controlsOrbit.mouseButtons={ORBIT:THREE.MOUSE.LEFT,ZOOM:THREE.MOUSE.RIGHT,PAN:THREE.MOUSE.MIDDLE},controlsOrbit.autoRotate=!0,controlsOrbit.autoRotateSpeed=2,controlsOrbit.enableKeys=!1,controlsOrbit.update(),controlsOrbit.enabled=!0}
