/**
 * @author Lasse Juul Kolding / http://dubbilan.net/
 */


function SceneInit() {

	//Add the container element to our html document
	container = document.createElement( 'div' );
	document.body.appendChild( container );

	///////////////
	// SCENE

	scene = new THREE.Scene();


	//////////////////
	// GROUND

    /*
	var groundGeo = new THREE.PlaneBufferGeometry( 20000, 20000 );
	var groundMat = new THREE.MeshPhongMaterial( { color: 0xffffff, specular: 0x050505 } );
	groundMat.color.setHSL( 0.095, 1, 0.75 );

	var ground = new THREE.Mesh( groundGeo, groundMat );
	ground.rotation.x = -Math.PI/2;
	ground.position.y = -10;
	scene.add( ground );

	ground.receiveShadow = true;
    */


	////////////////
	// LIGHTS

	//Setting up lights in Lights.js
	//threePointLight();
	LightsSetUp();


	///////////
	// FOG

	//scene.fog = new THREE.Fog( 0xffffff, 1, 7500 );
	//scene.fog.color.setHSL( 0.6, 0, 1 );

	///////////////
	// SKYDOME

	/*
	var vertexShader = document.getElementById( 'vertexShader' ).textContent;
	var fragmentShader = document.getElementById( 'fragmentShader' ).textContent;
	//  Sky Color
	var uniforms = {
		topColor: 	 { type: "c", value: new THREE.Color( 0x1689ff ) },
		bottomColor: { type: "c", value: new THREE.Color( 0x9ac9ff ) },
		offset:		 { type: "f", value: 0 },
		exponent:	 { type: "f", value: 1.2 }
	};

	uniforms.topColor.value.copy( hemiLight.color );

	//scene.fog.color.copy( uniforms.bottomColor.value );

	var skyGeo = new THREE.SphereGeometry( 4000, 32, 15 );
	var skyMat = new THREE.ShaderMaterial( {
		uniforms: uniforms,
		vertexShader: vertexShader,
		fragmentShader: fragmentShader,
		side: THREE.BackSide
	} );

	var sky = new THREE.Mesh( skyGeo, skyMat );

	scene.add( sky );
	*/


	///////////////////////////////
	//BACKGROUND Color
	
	scene.background = new THREE.Color( 0xffffff );

	/////////////////////////////////
	// CAMERAS / COLLISION / CONTROLS

	// Setup in CameraControls.js
	CamerasSetup();

	ControlsSetUp();

}
