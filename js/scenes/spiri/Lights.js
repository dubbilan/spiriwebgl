/**
 * @author Lasse Juul Kolding / http://dubbilan.net/
 */

var shadowMapSize = 512;
var camShadowDiam = 600;

function LightsSetUp() {

	//////////////////////////////
	// HemiSphere Light - Sky Light

	//hemiLight = new THREE.HemisphereLight( 0xffffff, 0xffffff, 0.6 );
	hemiLight = new THREE.HemisphereLight( 0xffffff, 0xffffff, 1.0 );
	//hemiLight.color.setHSL( 0.6, 1, 0.6 );
	//hemiLight.color.setHSL( 0.6, 1, 0.45 );
	hemiLight.color.setHSL( 0.6, 1, 0.3 );
	//hemiLight.groundColor.setHSL( 0.095, 0.35, 0.6 );
	hemiLight.groundColor.setHSL( 0.095, 0.1, 0.6 );
	hemiLight.position.set( 0, 500, 0 );
	scene.add( hemiLight );

	//////////////////////////
	// Dir Light - Shadow Caster

	dirLight = new THREE.DirectionalLight( 0xffffff, 0.5 );
	dirLight.color.setHSL( 0.1, 1, 0.95 );
	//dirLight.position.set( -1, 1.75, 1 );
	dirLight.position.set( -1, 2, 1 );
	dirLight.position.multiplyScalar( 400 );
	scene.add( dirLight );


	//Shadows on to begin with... remember renderer and mesh flags as well
	/*
	dirLight.castShadow = true;
	DirLightShadows(shadowMapSize, camShadowDiam)
	*/

	///////////////////////
	// Dir Light - Fill Light

	fillLight = new THREE.DirectionalLight( 0xffffff, 0.1 );
	fillLight.position.set( 0.5, 0.5, -1.0 );
	fillLight.position.multiplyScalar( 400 );
	scene.add( fillLight );

}

function DirLightShadows(mapsize, camdiam) {

	dirLight.shadow.mapSize.width = mapsize;
	dirLight.shadow.mapSize.height = mapsize;

	var d = camdiam;

	dirLight.shadow.camera.left = -d;
	dirLight.shadow.camera.right = d;
	dirLight.shadow.camera.top = d;
	dirLight.shadow.camera.bottom = -d;

	dirLight.shadow.camera.near = 1;
	dirLight.shadow.camera.far = 1500;
	dirLight.shadow.bias = -0.0001;

	//dirLight.shadowDarkness = 0.5;
	//dirLight.shadowCameraVisible = true;
}


/////////////////////
//////TOGGLES

function ToggleHemiLight() {

	hemiLight.visible = !hemiLight.visible;
}

function ToggleDirLight() {

	dirLight.visible = !dirLight.visible;
}

function ToggleShadow() {

	//console.log(scene.getObjectByName( "HouseGroup" ));

	//Cast Shadows or not
	shadowsOn = !shadowsOn;

	if (shadowsOn) {
		DirLightShadows(shadowMapSize, camShadowDiam);
	} else {
		DirLightShadows(0, 0);
	}

	var house = scene.getObjectByName( "Spiri" );
	house.traverse( function ( child ) {
		child.castShadow = shadowsOn;
	} );

	//comment these out to see the plane...
	dirLight.castShadow = shadowsOn;

	renderer.shadowMap.enabled = shadowsOn;

}


//////////////////
// Old Lights, before hemilight approach

function threePointLight() {

	directionalLight1 = new THREE.DirectionalLight( 0xaabbff, 0.3 ); //Orig Single Light
	//directionalLight1 = new THREE.DirectionalLight( 0xb8b8b8 );
	//directionalLight1.position.set(1, 1, 1).normalize();
	directionalLight1.position.set(0, 2, -2).normalize();
	directionalLight1.intensity = 1.0;
	scene.add( directionalLight1 );

	directionalLight2 = new THREE.DirectionalLight( 0xb8b8b8 );
	//directionalLight2.position.set(-1, 0.6, 0.5).normalize();
	directionalLight2.position.set(1.25, 0.6, -0.46).normalize();
	directionalLight2.intensity = 0.5;
	scene.add(directionalLight2);

	directionalLight3 = new THREE.DirectionalLight();
	//directionalLight3.position.set(-0.3, 0.6, -0.8).normalize( 0xb8b8b8 );
	directionalLight3.position.set(-1.25, 0.6, 1.13).normalize( 0xb8b8b8 );
	directionalLight3.intensity = 0.45;
	scene.add(directionalLight3);

	//Bottom light
	directionalLight4 = new THREE.DirectionalLight( 0xb8b8b8 );
	directionalLight4.position.set(-0.25, -0.6, -0.46).normalize();
	directionalLight4.intensity = 0.5;
	scene.add(directionalLight4);

}
