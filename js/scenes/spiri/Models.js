/**
 * @author Lasse Juul Kolding / http://dubbilan.net/
 */


//A Unit Box Geoemtry and Green Basic Material
/*
var geometry = new THREE.BoxGeometry( 100, 100, 100 );
//var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
var material = new THREE.MeshPhongMaterial( { color: 0x00ff00 } );
//A Mesh is an object that takes a geometry, and applies a material to it, which we then can insert to our scene, and move freely around.
cube = new THREE.Mesh( geometry, material );
//Add cube to Scene
cube.position.set(0,200,0);
scene.add( cube );
*/




/////////////////////
////SPIRI

function SpiriSetUp() {

	/*
	var onProgress = function ( xhr ) {
		if ( xhr.lengthComputable ) {
			var percentage = xhr.loaded / xhr.total * 100;
			console.log( percentage + '% loaded' );
			//SetPercentageValue(percentage*1.0);

			if (percentage == 100) {
				percentageCounter += 70;
				SetPercentageValue(percentageCounter);
				if (percentageCounter == 100) {
					ModelsDoneAddToScene();
				}

			}
		}
	};
	*/

	var onProgress = function ( xhr ) { };
	var onError = function ( xhr ) { console.log("ERROR") };

	////////////////
	//ENV SKYBOX
    /*
	var cubeSky;

	var r = "materials/textures/skybox/";
	var urls = [ r + "px.jpg", r + "nx.jpg",
				 r + "py.jpg", r + "ny.jpg",
				 r + "pz.jpg", r + "nz.jpg" ];

	cubeSky = new THREE.CubeTextureLoader().load( urls );
	cubeSky.format = THREE.RGBFormat;
	cubeSky.mapping = THREE.CubeReflectionMapping;
    */

	////////////////
	//ENV CHROME

	var cubeChrome;

	var r = "materials/textures/chrome/";
	var urls = [ r + "px.png", r + "nx.png",
				 r + "py.png", r + "ny.png",
				 r + "pz.png", r + "nz.png" ];

	cubeChrome = new THREE.CubeTextureLoader().load( urls );
	cubeChrome.format = THREE.RGBFormat;
	cubeChrome.mapping = THREE.CubeReflectionMapping;



	////////////////////
	////TEXTURES

	var texWindowGlass = new THREE.TextureLoader().load( "materials/textures/Translucent_Glass.jpg" );
	texWindowGlass.wrapS = THREE.RepeatWrapping;
	texWindowGlass.wrapT = THREE.RepeatWrapping;
	texWindowGlass.repeat.set( 3, 3 );

	var texShadowPlane = new THREE.TextureLoader().load( "materials/textures/ShadowPlane.jpg" );


	////////////////////////////

	var sceneMaterials = [];

	var ShadowPlane = new THREE.MeshBasicMaterial( {
		name: "ShadowPlane",
		color: 0xffffff,
		map: texShadowPlane,
		transparent: true,
		opacity: 1.0
	} );
	sceneMaterials.push( ShadowPlane );


    var ShadowCaster = new THREE.MeshLambertMaterial( {
		name: "ShadowCaster",
		transparent: true,
		opacity: 0.0
	} );
    sceneMaterials.push( ShadowCaster );

	////////////////////
	////SPIRI MATERIALS

    var SpiriBlue = new THREE.MeshPhongMaterial( {
		color: 0x9fdeed,
		specular: 0xffffff,
		envMap: cubeChrome,
		shininess: 8,
		reflectivity: 0.07,
		name: "SpiriBlue"
	} );
	sceneMaterials.push( SpiriBlue );

	var SpiriFace = new THREE.MeshLambertMaterial( {
		name: "SpiriFace",
		color: 0xffffff
	} );
	sceneMaterials.push( SpiriFace );

    var SpiriFeet = new THREE.MeshPhongMaterial( {
		name: "SpiriFeet",
		color: 0xffffff,
		specular: 0xffffff,
		envMap: cubeChrome,
		shininess: 5,
		reflectivity: 0.2
	} );
	sceneMaterials.push( SpiriFeet );

//0x5562c0
//0x2b566b
    var SpiriEyes = new THREE.MeshPhongMaterial( {
		color: 0x7fbaf0,
		specular: 0xffffff,
		envMap: cubeChrome,
		emissive: 0x2f6078,
		shininess: 50,
		reflectivity: 0.07,
		name: "SpiriEyes"
	} );
	sceneMaterials.push( SpiriEyes );

	var BlackPlastic = new THREE.MeshPhongMaterial( {
		name: "BlackPlastic",
		color: 0x000000,
		specular: 0xcccccc,
		shininess: 10
	} );
	sceneMaterials.push( BlackPlastic );

	var BlackSteel = new THREE.MeshPhongMaterial( {
		name: "BlackSteel",
		color: 0x111111,
		specular: 0x333333,
		shininess: 30
	} );
	sceneMaterials.push( BlackSteel );

	var BlackVents = new THREE.MeshPhongMaterial( {
		name: "BlackVents",
		color: 0x000000,
		specular: 0x000000,
		shininess: 10
	} );
	sceneMaterials.push( BlackVents );

	var CamGlass = new THREE.MeshStandardMaterial( {
		name: "CamGlass",
		color: 0x001111,
		map: texWindowGlass,
		envMap: cubeChrome,
		envMapIntensity: 50.0,
		transparent: true,
		opacity: 0.5
	} );
	sceneMaterials.push( CamGlass );

	var Carbon = new THREE.MeshPhongMaterial( {
		name: "Carbon",
		color: 0x111111,
		specular: 0xffffff,
		envMap: cubeChrome,
		shininess: 50,
		reflectivity: 0.7
	} );
	sceneMaterials.push( Carbon );


	//console.log(sceneMaterials.length);


	////////////////////
	////OBJ LOAD SPIRI

	var objLoader = new THREE.OBJLoader();
	objLoader.setPath( 'models/' );
	objLoader.load( 'Spiri_Web.obj', function ( object ) {

		//object.position.y = - 95;

		object.traverse( function ( child ) {
			if ( child instanceof THREE.Mesh ) {

				//Shadows

				if (child.material && child.material.name === "ShadowCaster") {
					child.castShadow = true;
					//child.receiveShadow = true;
				}


				if (child.material) {

					//Enabling MultiMaterial...
					if (child.material.type === "MultiMaterial") {
						for (var m=0; m < child.material.materials.length; m++) {
							//console.log(child.material.materials[m].name);
							for (var s=0; s < sceneMaterials.length; s++) {
								if (child.material.materials[m].name === sceneMaterials[s].name) {
									child.material.materials[m] =  sceneMaterials[s];
								}
							}
						}

					} else { //Straight single mesh materials

						//console.log(child.material.name);
						for (var s=0; s < sceneMaterials.length; s++) {
							if (child.material.name === sceneMaterials[s].name) {
								child.material =  sceneMaterials[s];
							}
						}
					}

				}
			}
		} );

		object.name = "Spiri";

        object.scale.multiplyScalar ( 1000 );

        object.rotation.y += Math.PI;

		//scene.add( object );
		meshesToAdd.push( object );

	}, onProgress, onError );


    ////////////////////
	////OBJ LOAD PROPS

	var objLoader = new THREE.OBJLoader();
	objLoader.setPath( 'models/' );
	objLoader.load( 'Spiri_Props.obj', function ( object ) {

		//object.position.y = - 95;

		object.traverse( function ( child ) {
			if ( child instanceof THREE.Mesh ) {

				if (child.material) {
					//console.log(child.material.name);
					for (var s=0; s < sceneMaterials.length; s++) {
						if (child.material.name === sceneMaterials[s].name) {
							child.material =  sceneMaterials[s];
						}
					}
				}
			}
		} );

        object.name = "Props";

        object.scale.multiplyScalar ( 1000 );

        object.rotation.y += Math.PI;

		//scene.add( object );
		meshesToAdd.push( object );

        //console.log( object);

	}, onProgress, onError );


	////////////////////
////OBJ LOAD SHADOW PLANE

var objLoader = new THREE.OBJLoader();
objLoader.setPath( 'models/' );
objLoader.load( 'Spiri_Shadow.obj', function ( object ) {

	//object.position.y = - 95;

	object.traverse( function ( child ) {
		if ( child instanceof THREE.Mesh ) {

			if (child.material) {
				//console.log(child.material.name);
				for (var s=0; s < sceneMaterials.length; s++) {
					if (child.material.name === sceneMaterials[s].name) {
						child.material =  sceneMaterials[s];
					}
				}
			}
		}
	} );

			object.name = "Shadow";

			object.scale.multiplyScalar ( 1000 );

			object.rotation.y += Math.PI;

	//scene.add( object );
	meshesToAdd.push( object );

			//console.log( object);

}, onProgress, onError );






}




////////////////////
//////ADD At The END

function ModelsDoneAddToScene() {

	console.log("MESHES to ADD", meshesToAdd.length)
	for (var index = 0; index < meshesToAdd.length; index++)
	{
		scene.add( meshesToAdd[index] );
		//console.log (meshesToAdd[index]);
	}

    //Add props to spinner arrau
    for (var propper = 0; propper < 4; propper++)
    {
        propsToSpin.push( scene.getObjectByName("Props").children[propper]);

    }

    //console.log( propsToSpin[0].position);


}
