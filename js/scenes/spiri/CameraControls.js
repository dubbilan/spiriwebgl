/**
 * @author Lasse Juul Kolding / http://dubbilan.net/
 */

////////////
////CAMERAS

 function CamerasSetup() {

	cameraOrbit = new THREE.PerspectiveCamera( 35, SCREEN_WIDTH / SCREEN_HEIGHT, 1, 10000 );
	cameraOrbit.position.set( 700, 200, 700 );

}




function ControlsSetUp() {

	///////////////////
	// CONTROLS ORBIT

	//controlsOrbit = new THREE.OrbitControls( cameraOrbit ); -- the missing (or default document) parameter for the controlsOrbit was the problem for missing text input!
	controlsOrbit = new THREE.OrbitControls( cameraOrbit, container );

	controlsOrbit.target = new THREE.Vector3(0,0,0);
	//controlsOrbit.enableDamping = true;
	controlsOrbit.minPolarAngle = 0.0;
	//controlsOrbit.maxPolarAngle = 0.9 * Math.PI / 2;
    controlsOrbit.maxPolarAngle = 0.9 * Math.PI;
	//controlsOrbit.enableZoom = false;
	controlsOrbit.enablePan = false;
	controlsOrbit.zoomSpeed = 0.5;
	controlsOrbit.minDistance = 600;
	controlsOrbit.maxDistance = 2000;
	controlsOrbit.enableKeys = false;
	controlsOrbit.mouseButtons = { ORBIT: THREE.MOUSE.LEFT, ZOOM: THREE.MOUSE.RIGHT, PAN: THREE.MOUSE.MIDDLE };

	controlsOrbit.autoRotate = true;
	controlsOrbit.autoRotateSpeed = 2.0;

	controlsOrbit.enableKeys = false;

	controlsOrbit.update();

	////////////////
	//Initially active

	controlsOrbit.enabled = true;
}
