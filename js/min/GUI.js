/**
 * @author Lasse Juul Kolding / http://dubbilan.net/
 */
function DivsUpdatePos(){var e,r;e=window.innerWidth/2-95+"px",r=window.innerHeight/2-10+"px",document.getElementById("progressBarHolder").style.left=e,document.getElementById("progressBarHolder").style.top=r}$(function(){isMobile||$(document).tooltip();var e=$("#progressbar"),r=$(".progress-label");e.progressbar({value:!1,change:function(){r.text(e.progressbar("value")+"%")},complete:function(){r.text("Done");$("#progressBarHolder").hide("fade",{},1e3)}}),SetPercentageValue=function(r){r=Math.round(r),e.progressbar("value",r),0==r&&$("#progressBarHolder").show("fade",{},200)}});
