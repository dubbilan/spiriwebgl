/**
 * @author Lasse Juul Kolding / http://dubbilan.net/
 */

///////////////////////////////////////////////////////
//////////////////  WINDOWS CHANGE/LAYOUT FUNCTIONS

function DivsUpdatePos() {

	var innerWidth = window.innerWidth;
	var innerHeight = window.innerHeight;

	var selectorLeft, selectorTop, selectorWidth, selectorHeight;

	//PROGRESS BAR

	selectorLeft = "" + (innerWidth / 2 - 95) + "px";
	selectorTop = "" + (innerHeight / 2 - 10) + "px";

	document.getElementById('progressBarHolder').style.left=selectorLeft;
	document.getElementById('progressBarHolder').style.top=selectorTop;

}



////////////////////
///////JQUERY UI


$( function() {

	//ToolTips
	if (!isMobile) {
		$( document ).tooltip();
	}

	//Empty options...Most effect types need no options passed by default
	var options = {};

	///////////////
	////Progress Bar

	var progressbar = $( "#progressbar" ),
	progressLabel = $( ".progress-label" );

	progressbar.progressbar({
	  value: false,
	  change: function() {
		progressLabel.text( progressbar.progressbar( "value" ) + "%" );
	  },
	  complete: function() {
		progressLabel.text( "Done" );

		//console.log("progress complete");

		//Fade out when complete
		var options = {};
		$( "#progressBarHolder" ).hide( "fade", options, 1000 );
	  }
	});


	function progress(val) {
		val = Math.round(val)
		progressbar.progressbar( "value", val);

		//console.log("progress val", val);

		//Fade in if value 0
		if (val == 0) {
			var options = {};
			$( "#progressBarHolder" ).show( "fade", options, 200 );
		}
		/*//Fade out if value 100 -- Fail Safe
		if (val == 100) {
			var options = {};
			$( "#progressBarHolder" ).hide( "fade", options, 1000 );
		}*/
	}

	SetPercentageValue = progress;

} );
